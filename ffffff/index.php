<?php
 include_once("vendor/autoload.php");

 use App\SEIP\ID_143764\module\About\about;
 use App\SEIP\ID_143764\module\Contact\contact;
 use App\SEIP\ID_143764\module\Images\images;
 use App\SEIP\ID_143764\module\Profile\profile;
 use App\SEIP\ID_143764\module\Users\users;

 $obj4Ab  = new about();
 $obj4con = new contact();
 $obj4img = new images();
 $obj4prfl = new profile();
 $obj4users = new users();
?>